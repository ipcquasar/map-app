import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    host: {'class': 'layout-column full-size direction-rtl'},
})
export class AppComponent {
}

import {HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
    ],
    providers: [],
    exports: [
    ],
})
export class SharedModule {
}

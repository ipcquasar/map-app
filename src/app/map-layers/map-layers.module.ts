import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MapComponent} from "./app/components/map/map.component";
import {LayersMenuComponent} from "./app/components/layers-menu/layers-menu.component";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {CustomCheckboxComponent} from "./app/components/custom.checkbox/custom.checkbox.component";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";

@NgModule({
    declarations: [
        MapComponent,
        LayersMenuComponent,
        CustomCheckboxComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        PerfectScrollbarModule,
    ],
    providers: [],
    exports: [
        MapComponent,
        LayersMenuComponent,
    ],
})
export class MapLayersModule {
}

export type Func = (...args: any[]) => any | void | Promise<any> | Promise<void>;

export interface ILayer {
    name: string;
    title: string;
    active: boolean;
}

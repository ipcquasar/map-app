import {
    Component, Input, Output, forwardRef, EventEmitter, AfterViewInit, ViewChild, TemplateRef
} from '@angular/core';
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from '@angular/forms';
import {getHash} from "../../../../shared/helpers";


const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomCheckboxComponent),
    multi: true
};


@Component({
    selector: 'custom-checkbox-ipc',
    templateUrl: './custom.checkbox.component.html',
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
    host: {'class': 'layout-row clickable content-gap10'}
})
export class CustomCheckboxComponent implements AfterViewInit {
    @Input() checked: boolean = false;
    @Input() disabled: boolean = false;
    @Input() displayOnly: boolean = false;

    @Output() onChange: EventEmitter<boolean> = new EventEmitter();

    @ViewChild('labelContent') labelContent: TemplateRef<any>;

    private onChangeCallback: (_: any) => void = noop;
    private onTouchedCallback: (_: any) => void = noop;

    private innerValue: boolean = false;
    public uid = getHash();
    public labelExists: boolean = true;

    constructor() {
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.labelExists = !!(this.labelContent as any)['nativeElement']['innerHTML'].length;
        });
    }


    get value(): any {
        return this.innerValue;
    }

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    //From ControlValueAccessor interface
    writeValue(value: any): void {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }

    changed(value: any): void {
        this.onChange.next(value);
    }
}

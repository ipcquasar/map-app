import {Component, OnInit} from "@angular/core";
import {ILayer} from "../../interfaces";
import {translations} from "../../contants";

@Component({
    selector: 'map-ipc',
    templateUrl: './map.component.html',
    host: {'class': 'layout-column'},
})
export class MapComponent implements OnInit {
    public govmap: any;
    public translations = translations;

    constructor() {
    }
    ngOnInit() {
        this.govmap = (window as any).govmap;
        if (!this.govmap) {
            throw new Error('Can\'t find the govmap app');
        }

        this.govmap.createMap('map',
            {
                token: '5a4b8472-b95b-4687-8179-0ccb621c7990',
                layers: [],
                showXY: true,
                identifyOnClick: true,
                isEmbeddedToggle: false,
                background: "1",
                layersMode: 4,
                zoomButtons:false
            });
    }

    public updateLayers(layers: ILayer[]) {
        const [layersOn, layersOff] = layers.reduce((accum, layer) => {
            accum[layer.active ? 0 : 1].push(layer.name);
            return accum;
        }, [[] as string[], [] as string[]]);

        this.govmap.setVisibleLayers(layersOn, layersOff);
    }
}

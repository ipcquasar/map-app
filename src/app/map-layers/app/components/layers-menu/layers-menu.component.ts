import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {ILayer} from "../../interfaces";
import {layers, translations} from "../../contants";

@Component({
    selector: 'layers-menu-ipc',
    templateUrl: './layers-menu.component.html',
    host: {'class': 'layout-column'},
})
export class LayersMenuComponent implements OnInit {
    @Output() onUpdate$: EventEmitter<ILayer[]> = new EventEmitter<ILayer[]>();
    public isExpanded: boolean = false;

    public layers: ILayer[] = [];
    public translations = translations;

    constructor(
    ) {
    }
    ngOnInit() {
        this.layers = Object.keys(layers).reduce((accum, layerKey) => {
            accum.push({
                name: layerKey,
                title: (layers as {[key: string]: string})[layerKey],
                active: false,
            })
            return accum;
        }, [] as ILayer[]);
    }

    public toggle() {
        console.log('>>>>>>>>>',);
        this.isExpanded = !this.isExpanded;
    }

    public updateLayers() {
        this.onUpdate$.next(this.layers);
    }
}

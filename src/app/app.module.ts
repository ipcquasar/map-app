import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './components/app/app.component';
import {SharedModule} from "./shared/shared.module";
import {MapLayersModule} from "./map-layers/map-layers.module";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        SharedModule,
        MapLayersModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
